package com.thehutgroup.accelerator;
import java.io.FileNotFoundException;
import java.util.Scanner;
/**
 * Main file for tic tac toe game.
 */
public class App 
{
    public static void main(String[] args ) throws FileNotFoundException
    {
        GameRunner game = new GameRunner();
        game.runGame();
    }
}
