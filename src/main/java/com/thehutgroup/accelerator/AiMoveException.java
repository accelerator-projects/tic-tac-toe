package com.thehutgroup.accelerator;

/**
 * A runtime exception that occurs when no move is found for the AI to perform.
 * This is usually due to broken logic in functions that ran prior to this
 * exception where the functions run but their conditional statements are never
 * met to output coordinates.
 */

public class AiMoveException extends RuntimeException {
    public AiMoveException(String message) {
        super(message);
    }

    public AiMoveException(String message, Throwable cause) {
        super(message, cause);
    }
}