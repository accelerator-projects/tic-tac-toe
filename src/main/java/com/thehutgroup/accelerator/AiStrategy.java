package com.thehutgroup.accelerator;

/**
 * An interface that any potential level of strategy can use to create
 * an algorithm for an AI to provide a move in a board game.
 */

public interface AiStrategy {
    public String makeMove();
}
