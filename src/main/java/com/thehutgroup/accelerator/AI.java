package com.thehutgroup.accelerator;


/**
 * An AI that plays this particular setup of tic tac toe
 */

public class AI extends Player {
    private AiStrategy strategy;

    /**
     * Constructor for the AI
     * @param piece the tic tac toe piece the AI is using
     * @param strategy the level of strategy defined by which class is passed in
     */

    public AI(Piece piece,String name,AiStrategy strategy) {
        super(piece,name);
        this.strategy = strategy;
    }


    /**
     * Performs a tic tac toe move according to its level of strategy.
     * @return the coordinates of its move.
     */

    @Override
    public String makeMove() {
        return strategy.makeMove();
    }
}
