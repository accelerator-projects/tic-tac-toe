package com.thehutgroup.accelerator;

/**
 * Enum for tic tac toe pieces.
 */

public enum Piece{
        O,
        X;

    /**
     * Returns the other piece
     * @param piece given piece
     * @return the other piece
     */

    public static Piece getOther(Piece piece) {
        switch (piece) {
            case O:
                return X;
            case X:
                return O;
            default:
                throw new RuntimeException("Invalid piece");
        }
    }
}