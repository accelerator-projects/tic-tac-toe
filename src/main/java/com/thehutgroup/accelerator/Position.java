package com.thehutgroup.accelerator;

import java.util.HashMap;
import java.util.Map;

/**
 * Defines x and y positions
 */

public class Position {
    private int x;
    private int y;
    private Map<String, String> desiredInputByArrayPosition = new HashMap<>();

    /**
     * A constructor for the position object
     * @param x x position
     * @param y y position
     */

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        desiredInputByArrayPosition.put("00","a1");
        desiredInputByArrayPosition.put("01","b1");
        desiredInputByArrayPosition.put("02","c1");
        desiredInputByArrayPosition.put("10","a2");
        desiredInputByArrayPosition.put("11","b2");
        desiredInputByArrayPosition.put("12","c2");
        desiredInputByArrayPosition.put("20","a3");
        desiredInputByArrayPosition.put("21","b3");
        desiredInputByArrayPosition.put("22","c3");
    }

    /**
     * Returns x position
     * @return x position
     */

    public int getX() {
        return x;
    }

    /**
     * Returns y position
     * @return y position
     */

    public int getY() {
        return y;
    }

    /**
     * Changes x and y position to be in format that is interpreted by the board
     * @return the position as a format that is interpreted by the board
     */

    public String convertToDesiredInput() {
        return desiredInputByArrayPosition.get(Integer.toString(this.x)+this.y);
    }
}
