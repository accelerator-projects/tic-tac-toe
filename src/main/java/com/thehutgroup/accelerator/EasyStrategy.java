package com.thehutgroup.accelerator;
import java.util.Map;
import java.util.Random;

/**
 * An algorithm for the AI to follow defined as easy.
 */

public class EasyStrategy extends AiMethods implements AiStrategy {
    private Board board;
    private Piece piece;

    /**
     * Constructor for the EasyStrategy class
     * @param board the current board setup
     * @param piece the turn player's piece
     */

    public EasyStrategy(Board board, Piece piece) {
        this.board = board;
        this.piece = piece;
    }

    /**
     * Implements the AIStrategy method to make a random move on the board.
     * @return
     */

    @Override
    public String makeMove() {
        return pickRandomMove(board,piece);
    }
}
