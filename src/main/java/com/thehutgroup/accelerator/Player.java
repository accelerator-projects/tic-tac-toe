package com.thehutgroup.accelerator;

/**
 * The boilerplate class for a generic player of a board game.
 */

public abstract class Player {
    private Piece piece;
    private String name;

    /**
     * The constructor for a player object.
     * @param piece
     * @param name
     */

    public Player(Piece piece,String name) {
        this.piece = piece;
        this.name=name;
    }

    /**
     * Returns the piece variable for the object
     * @return the piece variable
     */

    public Piece getPiece() {
        return piece;
    }

    /**
     * Returns the name variable for the object
     * @return the name variable
     */

    public String getName() {
        return name;
    }

    /**
     * A method that is overridden by a child object to perform a move
     * @return default string
     */
    public String makeMove() {return "default";}

    /**
     * A method that is overridden by a child object to perform a move
     * @return default string
     */

    public String makeMove(Board board) {return "default";}
}
