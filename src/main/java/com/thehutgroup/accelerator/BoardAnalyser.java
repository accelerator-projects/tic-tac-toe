package com.thehutgroup.accelerator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A class that contains methods that interact with board
 */

public class BoardAnalyser {
    Map<String, Integer> headerToArray;
    Board board;

    /**
     * Constructor for the BoardAnalyser object
     * @param board the current board setup
     */

    public BoardAnalyser(Board board) {
        headerToArray = new HashMap<>();
        headerToArray.put("a", 0);
        headerToArray.put("b", 1);
        headerToArray.put("c", 2);
        this.board = board;
    }

    /**
     * Checks if the inputted string by the user is of the desired format e.g. A1
     * @param input string provided by the user
     * @param piece turn player's piece
     * @return true if invalid input provided otherwise return false
     */

    public boolean testIfValidInput(String input, Piece piece) {
        try {
            if (input.charAt(0) == '-' && input.charAt(1) == '1' && input.charAt(2) == '-' && input.charAt(3) == '1') {
                board.save();
                System.exit(0);
            }
            if (input.length() != 2 || !(Character.toString(input.charAt(1)).matches("-?\\d+")) || !(Character.toString(input.charAt(0)).matches("[a-zA-Z]+"))) {
                System.out.println("This is not a valid input.");
                return true;
            }
            if (board.isSpaceOccupied(Integer.parseInt(Character.toString(input.charAt(1))) - 1, headerToArray.get(Character.toString(input.charAt(0)).toLowerCase()))) {
                System.out.println(Integer.parseInt(Character.toString(input.charAt(1))) - 1);
                System.out.println(headerToArray.get(Character.toString(input.charAt(0)).toLowerCase()));
                System.out.println(board.isSpaceOccupied(Integer.parseInt(Character.toString(input.charAt(1))) - 1, headerToArray.get(Character.toString(input.charAt(0)).toLowerCase())));
                System.out.println(board.board[0][0]);
                System.out.println("This space is occupied.");
                return true;
            }

            board.updateBoard(new int[]{Integer.parseInt(Character.toString(input.charAt(1))) - 1, headerToArray.get(Character.toString(input.charAt(0)).toLowerCase())}, piece);
            return false;
        } catch (Exception e) {
            System.out.println("This is not a valid input.");
            return true;
        }
    }

    /**
     * Checks if a player has won the game.
     * @param piece turn player's piece
     * @return true if the turn player has won otherwise return false
     */

    public boolean hasWonGame(Piece piece) {
        for (int i = 0; i < board.getLength(); i++) {
            if (testIfWon(i, piece, "columns") || testIfWon(i, piece, "rows") || testIfWon(i, piece, "diagonals")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the turn player has 3 in a row
     * @param columnIndex the column index for the row/the row index for the column
     * @param piece the turn player's piece
     * @param whatToTest describes whether checking a column, row or diagonal
     * @return true if player has 3 in a row otherwise false
     */

    public boolean testIfWon(int columnIndex, Piece piece, String whatToTest) {
        int pieceNumber = getPieceNumber(columnIndex, piece, whatToTest);

        if (pieceNumber == 3) {
            return true;
        }
        return false;
    }

    /**
     * Checks how many pieces a player has in a particular row/column/diagonal
     * @param columnIndex the column index for the row/the row index for the column
     * @param piece the turn player's piece
     * @param whatToTest describes whether checking a column, row or diagonal
     * @return number of pieces a player has in a particular row/column/diagonal
     */

    public int getPieceNumber(int columnIndex, Piece piece, String whatToTest) {
        int pieceNumber = 0;
        for (int i = 0; i < board.getLength(); i++) {
            switch (whatToTest) {
                case "columns":
                    if (board.isSpaceOccupiedBySpecificPiece(columnIndex, i, piece)) {
                        pieceNumber++;
                    }
                    break;
                case "rows":
                    if (board.isSpaceOccupiedBySpecificPiece(i, columnIndex, piece)) {
                        pieceNumber++;
                    }
                    break;
                case "diagonals":
                    pieceNumber = Arrays.stream(checkDiagonals(piece, pieceNumber)).max().getAsInt();
                    break;
            }
        }
        return pieceNumber;
    }

    /**
     * Checks how many pieces a player has in diagonals
     * @param piece the turn player's piece
     * @param pieceNumber the number of pieces a player has in a diagonal. Passed in to save time.
     * @return the number of pieces in both the left to right diagonal and the right to left diagonal
     */

    public int[] checkDiagonals(Piece piece, int pieceNumber) {
        if (pieceNumber > 0) {
            return new int[] {pieceNumber};
        }
        int leftDiagonal = 0;
        int rightDiagonal = 0;
        int[][] rightDiagonalPositions = {{2, 0}, {1, 1}, {0, 2}};
        for (int i = 0; i < board.getLength(); i++) {
            if (board.isSpaceOccupiedBySpecificPiece(i, i, piece)) {
                leftDiagonal++;
            }
            if (board.isSpaceOccupiedBySpecificPiece(rightDiagonalPositions[i][0], rightDiagonalPositions[i][1], piece)) {
                rightDiagonal++;
            }
        }
        return new int[] {leftDiagonal, rightDiagonal};
    }
}
