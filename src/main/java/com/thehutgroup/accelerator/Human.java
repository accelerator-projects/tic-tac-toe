package com.thehutgroup.accelerator;

import java.util.Scanner;

/**
 * The class for a human player of tic tac toe
 */

public class Human extends Player {

    /**
     * Constructor for human player.
     * @param piece human's piece
     * @param name name of the
     */

    public Human(Piece piece,String name) {
        super(piece,name);
    }

    /**
     * Takes in user input and attempts to place a piece on the board. Repeatedly asks for input until valid move is given.
     * @param board the current board setup
     * @return the valid place that the user wants to place their piece
     */

    @Override
    public String makeMove(Board board) {
        BoardAnalyser boardAnalyser = new BoardAnalyser(board);
        Scanner sc = new Scanner(System.in);
        System.out.println("It is " + this.getName() + "'s turn.");
        System.out.println("Enter your chosen coordinates e.g. A1");
        board.printBoard();
        String userInput = sc.next();
        while(boardAnalyser.testIfValidInput(userInput, this.getPiece())) {
            userInput=sc.next();
        }
        return userInput;
    }
}
