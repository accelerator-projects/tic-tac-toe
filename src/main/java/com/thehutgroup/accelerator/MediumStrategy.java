package com.thehutgroup.accelerator;
import java.util.Map;
import java.util.Random;

/**
 * The medium level strategy for the AI.
 */

public class MediumStrategy extends AiMethods implements AiStrategy {
    private Board board;
    private Piece piece;

    /**
     * The constructor for the medium strategy object.
     * @param board
     * @param piece
     */

    public MediumStrategy(Board board, Piece piece) {
        this.board = board;
        this.piece = piece;
    }

    /**
     * Performs the medium level algorithm for determing a move for the AI
     * @return the coordinate of the space decided by the algorithm
     */

    @Override
    public String makeMove() {
        return harderStrategy(board,piece,"medium");
    }
}
