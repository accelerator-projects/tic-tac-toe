package com.thehutgroup.accelerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * The methods the AI uses to decide how to make a move.
 */

public class AiMethods {
    private static Logger logger = LoggerFactory.getLogger(AiMethods.class);
    /**
     * Checks whether the current player is capable of winning on their next turn
     * @param board the current board setup
     * @param tests a list of the potential win conditions
     * @param piece the AI's piece
     * @return true/false depending if the human player can win on their next turn
     */

    public Optional<String> canIWin(Board board, String[] tests, Piece piece) {
        String validInputTest;
        int possibleSpace;
        for(int i=0;i<tests.length;i++)
        {
            for(int j=0;j<board.getLength();j++) {
                try {
                    possibleSpace = testIfCanWin(board, j, piece, tests[i]).get();
                }
                catch(NoSuchElementException e) {continue;}
                try{
                    validInputTest = testIfValidInput(possibleSpace,board,tests[i],piece).get();
                    return Optional.of(validInputTest);
                }
                catch(NoSuchElementException e) {}
            }
        }
        return Optional.empty();
    }

    /**
     * Checks whether the opponent can win this turn.
     * @param board the current board setup
     * @param tests a list of the potential win conditions
     * @param piece the AI's piece
     * @return true/false depending if the AI can win this turn
     */

    public Optional<String> canOpponentWin(Board board,String[] tests, Piece piece) {
        String validInputTest;
        int possibleSpace;
        for(int i=0;i<tests.length;i++)
        {
            for(int j=0;j<board.getLength();j++) {
                try {
                    possibleSpace = testIfCanWin(board, j, Piece.getOther(piece), tests[i]).get();
                }
                catch (NoSuchElementException e) {continue;}
                try {
                    validInputTest = testIfValidInput(possibleSpace, board, tests[i], piece).get();
                    return Optional.of(validInputTest);
                }
                catch(NoSuchElementException e) {}
            }
        }
        return Optional.empty();
    }

    /**
     * Makes any available move by finding a column/diagonal/row that contains an AI piece otherwise picks any empty space.
     * @param board the current board setup
     * @param tests a list of the potential win conditions
     * @param piece the AI's piece
     * @return the coordinate on the board fitting the described criteria
     */

    public Optional<String> pickAnyMove(Board board,String[] tests, Piece piece) {
        String validInputTest;
        int possibleSpace;
        for(int i=0;i<tests.length;i++)
        {
            for(int j=0;j<board.getLength();j++) {
                try {
                    possibleSpace = getAnyUsefulEmptySpace(board, j, piece, tests[i]).get();
                }
                catch(NoSuchElementException e) {continue;}
                try {
                    validInputTest = testIfValidInput(possibleSpace, board, tests[i], piece).get();
                }
                catch(NoSuchElementException e) {validInputTest=null;}
                if(validInputTest!="true")
                {
                    return Optional.of(validInputTest);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Depending on what string is passed in, checks the column/row/diagonal for the empty space within it.
     * @param board the current board setup
     * @param possibleSpace the position of the column/row/diagonal
     * @param tests either "columns", "rows" or "diagonals" depending on desired test
     * @param piece the current AI's piece
     * @return the coordinates of the empty space in the desired test row/column/diagonal
     */

    public Optional<String> findEmptySpace(Board board, int possibleSpace,String tests,Piece piece)
    {
        BoardAnalyser boardAnalyser = new BoardAnalyser(board);
        int[][] rightDiagonalPositions = {{2,0},{1,1},{0,2}};
        for(int j=0;j<board.getLength();j++)
        {
            switch(tests)
            {
                case "columns":
                    if(!board.isSpaceOccupied(possibleSpace,j))
                    {
                        Position position = new Position(possibleSpace,j);
                        return Optional.of(position.convertToDesiredInput());
                    }
                    break;
                case "rows":
                    if(!board.isSpaceOccupied(j,possibleSpace))
                    {
                        Position position = new Position(j,possibleSpace);
                        return Optional.of(position.convertToDesiredInput());
                    }
                    break;
                case "diagonals":
                    int[] numberOfPiecesInDiagonals = boardAnalyser.checkDiagonals(piece,0);
                    if(numberOfPiecesInDiagonals[1]>numberOfPiecesInDiagonals[0])
                    {
                        if(!board.isSpaceOccupied(rightDiagonalPositions[j][0],rightDiagonalPositions[j][1]))
                        {
                            Position position = new Position(rightDiagonalPositions[j][0],rightDiagonalPositions[j][1]);
                            return Optional.of(position.convertToDesiredInput());
                        }
                    }
                    else
                    {
                        if(!board.isSpaceOccupied(j,j))
                        {
                            Position position = new Position(j,j);
                            return Optional.of(position.convertToDesiredInput());
                        }
                    }
                    break;
            }
        }
        return Optional.empty();
    }

    /**
     * Tests whether there is a space in the desired column/row/diagonal.
     * @param possibleSpace the position of the column/row/diagonal
     * @param board the current board setup
     * @param tests either "columns", "rows" or "diagonals" depending on desired test
     * @param piece the current AI's piece
     * @return the coordinates of the space if it is empty otherwise returns empty optional
     */

    public Optional<String> testIfValidInput(int possibleSpace,Board board,String tests,Piece piece) {
        if (possibleSpace != -1) {
            try {
                String possiblyAlreadyFilled = findEmptySpace(board, possibleSpace, tests, piece).get();
                return Optional.of(possiblyAlreadyFilled);
            } catch (NoSuchElementException e) {}
        }
        return Optional.empty();
    }

    /**
     * Checks whether there are 2 current AI pieces in either a column, row or diagonal.
     * @param board the current board setup
     * @param columnIndex the index of the row for the column or the column for the row
     * @param piece the current AI's piece
     * @param whatToTest describes whether testing across column, row or diagonal
     * @return returns the index of the row for the column/column for the row if the column/row has 2 pieces in it otherwise
     * return -1
     */

    public Optional<Integer> testIfCanWin(Board board, int columnIndex, Piece piece,String whatToTest) {
        BoardAnalyser boardAnalyser = new BoardAnalyser(board);
        int pieceNumber = boardAnalyser.getPieceNumber(columnIndex,piece,whatToTest);
        if(pieceNumber==2)
        {
            return Optional.of(columnIndex);
        }
        return Optional.empty();
    }


    /**
     * Checks if the center space on the board is empty.
     * @param board the current board setup
     * @return the coordinate in the center of the board if it is empty otherwise returns an empty optional
     */

    public Optional<String> testCenter(Board board)
    {
        if(!board.isSpaceOccupied(1,1))
        {
            return Optional.of("b2");
        }
        return Optional.empty();
    }

    /**
     * Checks if there is a free corner to place a piece in.
     * @param board the current board setup
     * @param piece the current player's piece
     * @return the coordinate of the opposite corner to the opponent if it is free or any empty corner otherwise returns
     * an empty optional.
     */

    public Optional<String> testCorner(Board board,Piece piece)
    {
        int[][] corners = {{0,0},{0,2},{2,0},{2,2}};
        Piece opponentPiece = Piece.getOther(piece);
        Map<Integer, Integer> oppositeCorner = new HashMap<>();
        oppositeCorner.put(0,2);
        oppositeCorner.put(2,0);
        Random rand = new Random();
        for(int i=0;i<corners.length*3;i++)
        {
            int n = rand.nextInt(4);
            if(board.isSpaceOccupiedBySpecificPiece(corners[n][0],corners[n][1],opponentPiece) && board.isSpaceOccupied(oppositeCorner.get(corners[n][0]),oppositeCorner.get(corners[n][1])))
            {
                Position position = new Position(oppositeCorner.get(corners[n][0]),oppositeCorner.get(corners[n][1]));
                return Optional.of(position.convertToDesiredInput());
            }
            else if(board.isSpaceOccupied(corners[n][0],corners[n][1]))
            {
                Position position = new Position(corners[n][0],corners[n][1]);
                return Optional.of(position.convertToDesiredInput());
            }
        }
        return Optional.empty();
    }

    /**
     * Finds any column/row/diagonal that has at least one of the current player's pieces in it
     * @param board the current board setup
     * @param columnIndex the index of the row for the column/column for the row
     * @param piece the current player's piece
     * @param whatToTest describes whether checking a column, row or diagonal
     * @return the coordinates of an empty space on the board
     */

    public Optional<Integer> getAnyUsefulEmptySpace(Board board,int columnIndex, Piece piece,String whatToTest)
    {
        BoardAnalyser boardAnalyser = new BoardAnalyser(board);
        int pieceNumber = boardAnalyser.getPieceNumber(columnIndex, piece,whatToTest);
        if(pieceNumber>0)
        {
            return Optional.of(columnIndex);
        }
        return Optional.empty();
    }

    /**
     * Picks a truly random space on the board and repeats until it finds one that is unoccupied.
     * Will run forever if board is full.
     * @param board the current board setup
     * @param piece the current player's piece
     * @return the coordinates of an empty space.
     */

    public String pickRandomMove(Board board, Piece piece)
    {
        Random rand = new Random();
        int x;
        int y;
        do {
            x = rand.nextInt(3);
            y = rand.nextInt(3);
        }
        while(board.isSpaceOccupiedBySpecificPiece(x,y,piece) || board.isSpaceOccupiedBySpecificPiece(x,y,Piece.getOther(piece)));
        Position position = new Position(x,y);
        return position.convertToDesiredInput();
    }

    /**
     * Runs through multiple methods to find empty spaces based on the level of algorithm provided, following
     * a particular algorithmic pattern.
     * @param board the current board setup
     * @param piece the current player's piece
     * @param whichClass what level of difficulty this method will provide.
     * @return the coordinates of an empty space on the board determined to be the best available move
     */

    public String harderStrategy(Board board,Piece piece,String whichClass) {
        String[] tests = {"columns","rows","diagonals"};
        String result;
        try {
            logger.debug("Checking whether turn player has 2 in a column/row/diagonal and there is a space in the remaining spot");
            result = canIWin(board, tests, piece).get();
            return result;
        }
        catch(NoSuchElementException e){logger.warn("No two turn player's pieces in column/row/diagonal or no empty space in column/row/diagonal");}
        try {
            result = canOpponentWin(board, tests, piece).get();
            return result;
        }
        catch(NoSuchElementException e) {}
        if(whichClass=="hard") {
            String test;
            try {
                test = testCenter(board).get();
                return test;
            }
            catch(NoSuchElementException e) {}
            try{
                test = testCorner(board, piece).get();
                return test;
            }
            catch(NoSuchElementException e) {}
        }
        try{
            result = pickAnyMove(board,tests,piece).get();
            return result;
        }
        catch(NoSuchElementException e) {}
        return pickRandomMove(board,piece);
    }
}
