package com.thehutgroup.accelerator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * The hard strategy for the AI.
 */

public class HardStrategy extends AiMethods implements AiStrategy {
    private Board board;
    private Piece piece;

    /**
     * Constructor for the hard strategy object
     * @param board the board object
     * @param piece the AI's piece
     */

    public HardStrategy(Board board, Piece piece) {
        this.board = board;
        this.piece = piece;
    }

    /**
     * Runs the algorithm for the hard AI strategy
     * @return the coordinate of the move the algorithm has found
     */

    @Override
    public String makeMove() {
        return harderStrategy(board,piece,"hard");
    }


}