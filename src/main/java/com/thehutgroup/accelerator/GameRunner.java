package com.thehutgroup.accelerator;


import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * The class that holds the logic for running the main game loop.
 */

public class GameRunner {

    /**
     * Method that contains the main game loop.
     * @throws FileNotFoundException
     */

    public static void runGame() throws FileNotFoundException {
        while(true) {
            Board board = Board.getInstance();
            BoardAnalyser boardAnalyser = new BoardAnalyser(board);
            Scanner sc = new Scanner(System.in);
            Boolean hasBoardReloadHappened = false;
            while(true) {
                System.out.println("Would you like to load the previous board? Enter Y/N");
                String input = sc.next();
                if(input.toLowerCase().equals("y"))
                {
                    board.reload();
                    hasBoardReloadHappened=true;
                    break;
                }
                else if (input.toLowerCase().equals("n"))
                {
                    break;
                }
                System.out.println("Please enter y/n.");
            }
            System.out.println("Single player/Multi-player game? s/m?");
            Player player1;
            Player player2;
            while(true) {
                String input = sc.next();
                if (input.toLowerCase().equals("s")) {
                    AiStrategy strategy = getDifficultyLevel(board,sc);
                    player1 = new AI(Piece.O,"AI",strategy);
                    player2 = new Human(Piece.X,"Human");
                    break;
                } else if (input.toLowerCase().equals("m")) {
                    if(hasBoardReloadHappened)
                    {
                        Piece turnPlayer = Piece.getOther(findWhosTurn(board));
                        player1 = new Human(turnPlayer,"Player 1");
                        player2 = new Human(Piece.getOther(turnPlayer),"Player 2");
                    }
                    else {
                        player1 = new Human(Piece.X,"Player 1");
                        player2 = new Human(Piece.O, "Player 2");
                    }
                    break;
                }
                System.out.println("That is not a valid input.");
            }
            board.resetBoard();
            while (!boardAnalyser.hasWonGame(player2.getPiece()) && !(board.isFull())) {
                if(player1 instanceof Human)
                {
                    System.out.println("first");
                    boardAnalyser.testIfValidInput(player1.makeMove(board),player1.getPiece());
                }
                else
                {
                    System.out.println("secind");
                    boardAnalyser.testIfValidInput(player1.makeMove(),player1.getPiece());
                }
                Player justPlayed = player1;
                player1=player2;
                player2=justPlayed;
            }
            board.printBoard();
            if (board.isFull() && !boardAnalyser.hasWonGame(player2.getPiece())) {
                System.out.println("DRAW! Someone please be better!");
            } else {
                System.out.println("Congrats, " + player2.getName() + " wins. Take all the time you need to gloat!");
            }
            System.out.println("Would you like to play again? Y/N");
            String input = sc.next();
            if (input.toLowerCase().equals("n")) {
                break;
            }
        }
    }

    /**
     * Takes in the difficulty level of the AI the player wants to play against
     * @param board the board object
     * @param sc the Scanner to take in user input
     * @return the difficulty level of the AI as a strategy object
     */

    public static AiStrategy getDifficultyLevel(Board board, Scanner sc) {
        Piece piece = Piece.values()[1];
        AiStrategy strategy;
        while (true) {
            System.out.println("What difficulty would you like? Easy, medium or hard? Enter E/M/H.");
            String input = sc.next();
            if (input.toLowerCase().equals("e")) {
                strategy = new EasyStrategy(board, Piece.getOther(piece));
                break;
            } else if (input.toLowerCase().equals("m")) {
                strategy = new MediumStrategy(board, Piece.getOther(piece));
                break;
            } else if (input.toLowerCase().equals("h")) {
                strategy = new HardStrategy(board, Piece.getOther(piece));
                break;
            }
            System.out.println("This is not a valid input");
        }
        return strategy;
    }

    /**
     * Tries to guess who's turn it was when the game stopped when reloading a previous board save.
     * @param board the reloaded board setup
     * @return the player's turn piece
     */

    public static Piece findWhosTurn(Board board) {
        int OPieces = 0;
        int XPieces = 0;
        for(int i=0;i<board.getLength();i++)
        {
            for(int j=0;j<board.getLength();j++)
            {
                if(board.isSpaceOccupiedBySpecificPiece(i,j,Piece.O))
                {
                    OPieces++;
                }
                else if(board.isSpaceOccupiedBySpecificPiece(i,j,Piece.X))
                {
                    XPieces++;
                }
            }
        }
        if(OPieces>XPieces)
        {
            return Piece.X;
        }
        else
        {
            return Piece.O;
        }
    }
}