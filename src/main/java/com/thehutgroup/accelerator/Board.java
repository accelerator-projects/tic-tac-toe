package com.thehutgroup.accelerator;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * The class that contains where all the pieces currently in play are.
 */

public class Board {
    public Piece[][] board;
    Map<String, Integer> headerToArray;
    private static Board singleInstance = null;

    @Override
    public String toString() {
        return "Board{" +
                "board=" + Arrays.toString(board) +
                '}';
    }

    /**
     * The builder for the Board class.
     * @param builder builder for the Board class.
     */

    public Board(Builder builder) {
        this.board = builder.board;
        this.headerToArray = builder.headerToArray;
    }

    /**
     * Creates a Board object if one doesn't already exist.
     * @return the board object.
     */

    public static Board getInstance() {
        if(singleInstance==null) {
            Map<String, Integer> map = new HashMap<>();
            map.put("a",0);
            map.put("b",1);
            map.put("c",2);
            singleInstance = Board.Builder.newInstance().addBoard(new Piece[3][3]).addHashMap(map).build();
        }
        return singleInstance;
    }

    /**
     * returns the width of the board row.
     * @return width of the board row.
     */

    public int getLength() {
        return board.length;
    }

    /**
     * Checks whether a particular space is empty
     * @param x x-coordinate of the board
     * @param y y-coordinate of the board
     * @return true if the space contains a piece otherwise returns false
     */

    public boolean isSpaceOccupied(int x,int y) {
        return board[x][y]==null ? false : true;
    }

    /**
     * Checks whether a particular space is occupied by a particular piece
     * @param x x-coordinate of the board
     * @param y y-coordinate of the board
     * @param piece player piece that is being looked for
     * @return true if the particular piece is in that space otherwise false
     */

    public boolean isSpaceOccupiedBySpecificPiece(int x, int y, Piece piece)
    {
        return board[x][y]==piece ? true : false;
    }

    /**
     * Add a piece in a particular spot on the board
     * @param validInput contains x and y coordinate of the board
     * @param piece the turn player's piece
     */

    public void updateBoard(int[] validInput,Piece piece){
       board[validInput[0]][validInput[1]] = piece;
    }

    /**
     * Removes all pieces from the board.
     */

    public void resetBoard() {
        for(int i=0;i<board.length;i++)
        {
            for(int j=0;j<board.length;j++)
            {
                board[i][j]=null;
            }
        }
    }

    /**
     * Prints the contents of the board to the screen in human readable terms.
     */

    public void printBoard() {
        System.out.println("    A B C");
        for(int i=0;i<board.length;i++)
        {
            System.out.print(i+1 + "->");
            System.out.print("|");
            for(int j=0;j<board[0].length;j++)
            {
                if(board[i][j]==null)
                {
                    System.out.print(" ");
                }
                else
                {
                    System.out.print(board[i][j]);
                }

                System.out.print("|");
            }
            System.out.printf("%n");
            System.out.println("   -------");
        }
    }

    /**
     * Checks whether the board is full
     * @return true if board is full otherwise false
     */

    public boolean isFull() {
        for(int i=0;i<board.length;i++)
        {
            for(int j=0;j<board.length;j++)
            {
                if(board[i][j]==null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Saves the current board setup to a file.
     * @throws FileNotFoundException
     */

    public void save() throws FileNotFoundException {
        File file = new File("../../../resources/savedBoard.txt");
        PrintWriter out = new PrintWriter(file);

        for(int i = 0; i<board.length; i++)
        {
            for (int j = 0; j<board.length; j++)
            {
                out.print(board[i][j]);
                out.print(" ");
            }
            out.println();
        }
        out.close();
    }

    /**
     * Loads the board from a file.
     * @throws FileNotFoundException
     */

    public void reload() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("../../../../resources/savedBoard.txt")));
        int rows = 4;
        int columns = 4;
        Piece [][] savedBoard = new Piece[3][3];
        while(sc.hasNextLine()) {
            for (int i=0; i<savedBoard.length; i++) {
                String[] line = sc.nextLine().trim().split(" ");
                for (int j=0; j<line.length; j++) {

                    if(line[j].equals("null"))
                    {
                        savedBoard[i][j] = null;
                    }
                    else if(line[j].equals("O"))
                    {
                        savedBoard[i][j] = Piece.O;
                    }
                    else if (line[j].equals("X"))
                    {
                        savedBoard[i][j] = Piece.X;
                    }
                    else
                    {
                        System.out.println(line[i]);
                        throw new RuntimeException("Something went wrong");
                    }

                }
            }
        }
        board = savedBoard;
    }

    /**
     * Builder class for the Board.
     */

    public static class Builder {
        private Piece[][] board = new Piece[3][3];
        Map<String, Integer> headerToArray = new HashMap<>();
        public Builder() {}
        public static Builder newInstance()
        {
            return new Builder();
        }
        public Builder addBoard(Piece[][] board) {
            this.board = board;
            return this;
        }
        public Builder addHashMap(Map<String,Integer> map) {
            this.headerToArray = map;
            return this;
        }
        public Board build() {
            return new Board(this);
        }
    }
}
